// dependencies
require('dotenv').config()
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// routes
const userRoute = require('./routes/userRoutes');
const productRoute = require('./routes/productRoutes');

// express app
const app = express();

// global middlewares
app.use(cors());
app.use(express());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// route handler
app.use('/api/user', userRoute);
app.use('/api/products', productRoute);

// db connection
mongoose.set('strictQuery', true)

mongoose
	.connect(process.env.MONGO_URI)
	.then(() => {
		app.listen(process.env.PORT, () => {
			console.log(`Connected to database.`)
			console.log(`Server running...`)
		});
	})
	.catch(error => {
		console.log(error)
		console.log(`WARNING: Connection error.`)
	})