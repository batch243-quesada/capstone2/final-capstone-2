const mongoose = require('mongoose');

const productSchema = new mongoose.Schema(
		{
			title: { type: String, required: true },
			description: { type: String, required: true },
			// img: { type: String, required: true },
			// categories: { type: Array },
			price: { type: Number, required: true },
			stock: { type: Number, required: true },
			isActive: { type: Boolean, default: true }
		},
		{
			timestamps: true
		}
	)

module.exports = mongoose.model("Product", productSchema);
