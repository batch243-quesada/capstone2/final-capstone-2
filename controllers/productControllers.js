// import modules
const Product = require('../models/Products');
const User = require('../models/Users');
const	authen = require('../authen');

// Create a Product
const createProduct = (request, response) => {
	let newProduct = new Product(
			{
				title: request.body.title,
				description: request.body.description,
				price: request.body.price,
				stock: request.body.stock
			}
		)

	// Verify if Admin
	const userData = authen.decode(request.headers.authorization);
	if(userData.isAdmin) {
		return newProduct.save()
		.then(result => {
			console.log(`Added to inventory: ${newProduct.title}`)
			response.send(`Added to inventory: ${newProduct.title} - ${newProduct.stock}pcs`);
		}).catch(error => {
			console.log(error);
			response.send(false);
		})
	} else response.send('Access denied.');
}

// Retrieve All Active Products
const getAllActive = (request, response) => {
	return Product.find({isActive : true})
	.then(result => {
		console.log(`Rendering available products.`)
		response.send(result);
	}).catch(err => {
		response.send(err);
	})
}

// Retrieve All Products
const getAllProducts = (request, response) => {
	const userData = authen.decode(request.headers.authorization);

	if(!userData.isAdmin) {
		return response.send(`Sorry, you don't have access to this page.`);
	} else {
		return Product.find({})
		.then(result => response.send(result))
		.catch(err => {
			console.log(err);
			response.send(err);
		})
	}
}

// Retrieve Specific Product
const getProduct = (request, response) => {
	const productId = request.params.productId;

	return Product.findById(productId).then(result => {
		console.log(`Rendering ${result.title}`)
		response.send(result);
	}).catch(err => {
		response.send(err);
	})
}

// Update a product
const updateProduct = (request, response) => {
	const userData = authen.decode(request.headers.authorization);

	let updatedProduct = {
		title: request.body.title,
		description: request.body.description,
		price: request.body.price,
		stock: request.body.stock
	}

	const productId = request.params.productId;
	if(userData.isAdmin) {
		return Product.findByIdAndUpdate(productId, updatedProduct, {new:true})
		.then(result => {
			response.send(result);
		}).catch(err => {
			response.send(err);
		})
	} else {
		return response.send("Access denied.");
	}
}

// Archive / Unarchive Product
const archiveProduct = (request, response) => {
	const userData = authen.decode(request.headers.authorization);
	const productId = request.params.productId;

	const subjectProduct = Product.findById(productId)
	.then(subjectProduct => {
		let archivedProduct = {
			isActive: !subjectProduct.isActive
		}

		if (userData.isAdmin) {
			return Product.findByIdAndUpdate(productId, archivedProduct, {new:true})
			.then(result => {
				console.log(result);
				if(!result.isActive) response.send(`Archived: ${result.title}`)
				else response.send(`Retrieved from Archive: ${result.title}`)
			}).catch(err =>{
				response.send(err);
			})
		} else {
			response.send('Access denied.')
		}
	}).catch(err =>{
		response.send(err);
	});
}

module.exports = {
	createProduct,
	getAllActive,
	getAllProducts,
	getProduct,
	updateProduct,
	archiveProduct
}