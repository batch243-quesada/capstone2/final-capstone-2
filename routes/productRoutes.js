// dependencies
const router = require('express').Router();

// imports
const Product = require('../models/Products');
const productController = require('../controllers/productControllers');
const authen = require('../authen');

//// ROUTES
// create products
router.post('/add-to-inventory', authen.verify, productController.createProduct);

// retrieve active products
router.get("/on-stock", productController.getAllActive);

// retieve all products
router.get("/all-products", authen.verify, productController.getAllProducts);

// retrieve single product
router.get("/:productId", productController.getProduct);

// update a product
router.put("/update/:productId", authen.verify, productController.updateProduct);

// archive/unarchive product
router.patch("/update/:productId/archive", authen.verify, productController.archiveProduct);


module.exports = router;